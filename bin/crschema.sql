--
-- 
--
--
\set ON_ERROR_STOP off
\set ECHO all
--
\set v_schema test
\echo :v_schema
--
SELECT  'create schema ' || :v_schema::text || ' ;'
\gexec

--
select 'create role role_' || :v_schema::text || '_' || rname::text || ' with nologin;' as sql from (values ('ro'),('rw'),('adm')) t1(rname)
\gexec
--
-- cleanup
select 'drop role role_' || :v_schema::text || '_' || rname::text || ' ;' as sql from (values ('ro'),('rw'),('adm')) t1(rname)
\gexec

SELECT  'drop schema ' || :v_schema::text || ' ;'
\gexec
