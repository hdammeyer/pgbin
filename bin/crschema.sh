#!/usr/local/bin/bash
#

# set -ex

usage() {
  echo ""
  echo "Usage:"
  echo ""
  echo "$0 <host> <db> <schema>"
  echo ""
}

if [ "$#" -ne 3 ];then
  usage
  exit 1
fi
export V_HOST=${1}
export V_DB=${2}
export V_SCHEMA=${3}
export V_SCHEMA=${V_SCHEMA,,}

psql -h $V_HOST -p 5432 -U postgres $V_DB \
  -v v_schema="'${V_SCHEMA,,}'" \
  -f create_schema.sql
