--
--     Script:         crroles.sql
--     Author:         Heiko Dammeyer
--     Date:           17.04.2019
--     Purpose:        
--
--
--     Usage:
--
--     Notes:
--       Buckets
--
--
--

\set v_schema_name neu
\echo :v_schema_name


with rolnames as (select 'role_' || :'v_schema_name'::text || '_' || rname::text as schema_role from (values ('ro'),('rw'),('adm')) t1(rname) )
select 'create role ' || schema_role || ' ;' as sql from rolnames where schema_role not in (select rolname from pg_roles) \g


--

