--
--     Script:         crusrdb.sql
--     Author:         Heiko Dammeyer
--     Date:           05.04.2019
--     Purpose:        
--
--
--     Usage:
--
--     Notes:
--       Buckets
--
--
--
\set ECHO all
--
drop table if exists tmp_usrdb;
create table tmp_usrdb (username text, password text,description text,db text);
\copy tmp_usrdb from 'crusrdb.csv' csv header ;
select 'create user ' || username || ' with login password ' || password ||' ;' 
from tmp_usrdb
\g
select 'grant connect on database ' || db || ' to ' || username ||' ;' 
from tmp_usrdb
\g
drop table if exists tmp_usrdb;

