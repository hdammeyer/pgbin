--
-- DB erstellen
-- Schema erstellen 
--
\set ON_ERROR_STOP on
\set ECHO all

\conninfo

\set v_db_name 'test_heiko'

SELECT current_setting('cluster_name') as "v_cluster_name" \gset
SELECT :'v_cluster_name' || '_adm' as "v_cluster_adm" \gset

--
-- drop database if exists :v_db_name \g
create database :v_db_name  \g
comment on database :v_db_name is 'Generische DB by Heiko' \g
alter database :v_db_name owner to :v_cluster_adm \g
grant all on database :v_db_name to :v_cluster_adm \g
-- alter database :v_db_name set search_path = :v_db_name, public \g
--
--

revoke all on database :v_db_name from public;
\l+ :v_db_name

--
--
\c :v_db_name :v_cluster_adm
\conninfo
\set v_schema_name :v_cluster_name
-- \ir crroles.sql 
-- \ir create_schema.sql
create schema :v_schema_name authorization :v_cluster_adm  \g
\set v_desc 'Default Schema in DB ':v_db_name' !!'
comment on schema :v_schema_name is :'v_desc' \g
grant all on schema :v_schema_name to :v_cluster_adm \g
revoke all on schema public from public \g
alter database :v_db_name set search_path = :v_db_name, public \g

--
-- fuer den zugriff auf das schema muss ein connect auf die db moeglich sein
select 'role_' || :'v_cluster_name' || '_rw' as "v_cluster_role_ro" \gset
grant connect on database :v_db_name to :v_cluster_role_ro \g
grant usage on schema :v_schema_name to :v_cluster_role_ro \g
select 'role_' || :'v_cluster_name' || '_ro' as "v_cluster_role_rw" \gset
grant connect on database :v_db_name to :v_cluster_role_rw \g
grant usage on schema :v_schema_name to :v_cluster_role_rw \g
select 'role_' || :'v_cluster_name' || '_adm' as "v_cluster_role_adm" \gset
grant connect on database :v_db_name to :v_cluster_role_adm \g
--

--
\l+  :v_db_name
\dn+ :v_schema_name
-- \du+ :v_schema_name
--
